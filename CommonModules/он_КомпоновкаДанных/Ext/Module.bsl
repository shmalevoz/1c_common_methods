﻿
//
// Методы работы с компоновкой данных
// 

//
// Параметры:  
// 

// Возвращает значение параметра настроек компоновки, НЕОПРЕДЕЛЕНО в случае отсутствия
//
// Параметры:  
//	Параметры	- колелкция параметров
//	Имя			- имя параметра
//
// Возвращаемое значение:
//   Произвольный|Неоределено
//
Функция ПараметрЗначениеПолучить(Параметры, Имя) Экспорт
	
	Параметр	= ПараметрПолучить(Параметры, Имя, Ложь);
	
	Возврат ?(Параметр = Неопределено, Неопределено, Параметр.Значение);
	
КонецФункции //ПараметрЗначениеПолучить 

// Возвращает значение используемого параметра, Неопределено в случае отсутствия или не использования
//
// Параметры:  
// 	Параметры
// 	Имя
//
// Возвращаемое значение:
//   Произвольный	- значение параметра|Неопределено
//
Функция ПараметрИспользуемыйЗначениеПолучить(Параметры, Имя) Экспорт
	
	Параметр	= ПараметрПолучить(Параметры, Имя, Ложь);
	
	Возврат ?(Параметр <> Неопределено И Параметр.Использование, Параметр.Значение, Неопределено);
	
КонецФункции //ПараметрИспользуемыйЗначениеПолучить 

// Возвращает параметр настройки компоновки данных, НЕОПРЕДЕЛЕНО в случае отсутствия
//
// Параметры:  
//	Параметры	- коллекция параметров
//	Имя			- имя параметра
//
// Возвращаемое значение:
//   Произвольный|Неопределено
//
Функция ПараметрПолучить(Параметры, Имя, Создание = Ложь) Экспорт
	
	Параметр	= Параметры.НайтиЗначениеПараметра(Новый ПараметрКомпоновкиДанных(Имя));
	Если Параметр = Неопределено И Создание Тогда
		Параметр	= Параметры.Элементы.Добавить();
		Параметр.Параметр	= Новый ПараметрКомпоновкиДанных(Строка(Имя));
	КонецЕсли; 
	
	Возврат Параметр;
КонецФункции //ПараметрПолучить 

// Устанавливает значение параметра настроек СКД. Возвращает успех выполнения
//
// Параметры:  
//	Настройки
//	Имя
//	Значение
//
// Возвращаемое значаение
//	Булево	- успех установки параметра
//
Функция ПараметрЗначениеУстановить(Параметры, Имя, Значение, Создание = Истина) Экспорт
	
	Параметр		= ПараметрПолучить(Параметры, Имя, Создание);
	ПараметрЕсть	= Параметр <> Неопределено;
	
	Если ПараметрЕсть Тогда
		Параметр.Использование	= Истина;
		Параметр.Значение		= Значение;
	КонецЕсли; 
	
	Возврат ПараметрЕсть;
	
КонецФункции //ПараметрЗначениеУстановить 

// Устанавливает параметр с проверкой его типа
//
// Параметры:  
//	Параметры
//	Имя
//	Значение
//
Процедура ПараметрТипизованныйУстановить(Параметры, Имя, Значение) Экспорт
	
	Параметр	= он_КомпоновкаДанных.ПараметрПолучить(Параметры, Имя);
	Если Параметр <> Неопределено И ТипЗнч(Параметр.Значение) = ТипЗнч(Значение) Тогда
		Параметр.Использование	= Истина;
		Параметр.Значение		= Значение;
	КонецЕсли; 
	
КонецПроцедуры //ПараметрТипизованныйУстановить 

// Возвращает массив имен обязательных параметров найстроки компоновки данных
//
// Параметры:  
//	Настройка
//	Преобразовать в нижний регистр
//
// Возвращаемое значение:
//   Массив
//
Функция ПараметрыОбязательныеИменаПолучить(Настройка, ПреобразоватьВНижнийРегистр = Ложь) Экспорт
	
	Результат	= Новый Массив;
	
	Для каждого Элемент Из Настройка.ПараметрыДанных.ДоступныеПараметры.Элементы Цикл
		Если Элемент.ЗапрещатьНезаполненныеЗначения Тогда
			Результат.Добавить(?(ПреобразоватьВНижнийРегистр, НРег(Строка(Элемент.Параметр)), Строка(Элемент.Параметр)));
		КонецЕсли; 
	КонецЦикла; 
	
	Возврат Результат;
КонецФункции //ПараметрыОбязательныеИменаПолучить 

//
// Отбор
// 

// Возвращает новый элемент отбора данных
//
// Параметры:  
// 	ПутьКДанным
// 	Значение
// 	ВидСравнения
//
// Возвращаемое значение: 
// 	ЭлементОтбораКомпоновкиДанных
//
Функция ОтборЭлементДобавить(Отбор, ПутьКДанным, Значение, ВидСравнения = Неопределено, Использование = Истина) Экспорт
	
	Результат	= Отбор.Элементы.Добавить(Тип("ЭлементОтбораКомпоновкиДанных"));
	Результат.ЛевоеЗначение		= Новый ПолеКомпоновкиДанных(ПутьКДанным);
	Результат.ПравоеЗначение	= Значение;
	Результат.ВидСравнения		= ?(ВидСравнения = Неопределено, ВидСравненияКомпоновкиДанных.Равно, ВидСравнения);
	Результат.Использование		= Использование;
	
	Возврат Результат;
	
КонецФункции //ОтборЭлементДобавить 

// Возвращает новую группу отбора данных
//
// Параметры:  
// 	Отбор
// 	ТипГруппы:ГруппаЭлементовОтбораКомпоновкиДанных - по-умолчанию ТипГруппыЭлементовОтбораКомпоновкиДанных.ГруппаИ
//
// Возвращаемое значение: 
// 	ТипГруппыЭлементовОтбораКомпоновкиДанных
//
Функция ОтборГруппаДобавить(Отбор, ТипГруппы = Неопределено) Экспорт
	
	Результат	= Отбор.Элементы.Добавить(Тип("ГруппаЭлементовОтбораКомпоновкиДанных"));
	
	Результат.ТипГруппы	= ?(ТипГруппы = Неопределено, ТипГруппыЭлементовОтбораКомпоновкиДанных.ГруппаИ, ТипГруппы);
	
	Возврат Результат;
	
КонецФункции //ОтборГруппаДобавить 

// Возвращает элемент отбор компоновки по имени|Неопределено
//
// Параметры:  
//	Отбор
//	Имя
//
// Возвращаемое значение:
//   ЭлементОтборКомпоновкиДанных|Неопределено
//
Функция ОтборЭлементНайти(Отбор, ПутьКДанным) Экспорт
	
	Результат	= Неопределено;
	Искомое		= НРег(ПутьКДанным);
	
	Для каждого Элемент Из Отбор.Элементы Цикл
		Если ТипЗнч(Элемент) = Тип("ЭлементОтбораКомпоновкиДанных") Тогда
			Если НРег(Элемент.ЛевоеЗначение) = Искомое Тогда
				Результат	= Элемент;
				Прервать;
			КонецЕсли; 
		Иначе 
			Результат	= ОтборЭлементНайти(Элемент, ПутьКДанным);
			Если Результат <> Неопределено Тогда
				Прервать;
			КонецЕсли; 
		КонецЕсли; 
	КонецЦикла; 
	
	Возврат Результат;
КонецФункции //ОтборЭлементНайти 

// Устанавливает значаение отбора компоновки данных
//
// Параметры:  
//	Настройки
//	ПутьКДанным
//	Значение
//	ВидСравнения
//  РежимОтображения
//	Замещение
//  Использование
//
Процедура ОтборЗначениеУстановить(Отбор, ПутьКДанным, Значение
	, ВидСравнения = Неопределено, РежимОтображения = Неопределено
	, Замещение = Истина, Использование = Истина, Заголовок = Неопределено) Экспорт
	
	Элемент	= ОтборЭлементНайти(Отбор, ПутьКДанным);
	Если НЕ Замещение ИЛИ Элемент = Неопределено Тогда
		Элемент	= Отбор.Элементы.Добавить(Тип("ЭлементОтбораКомпоновкиДанных"));
	КонецЕсли; 
	
	Элемент.ЛевоеЗначение	= Новый ПолеКомпоновкиДанных(ПутьКДанным);
	Элемент.Использование	= Использование;
	Элемент.ВидСравнения	= ?(ВидСравнения = Неопределено
	, ?(ТипЗнч(Значение) = Тип("Массив"), ВидСравненияКомпоновкиДанных.ВСписке, ВидСравненияКомпоновкиДанных.Равно)
	, ВидСравнения);
	Элемент.ПравоеЗначение	= Значение;
	Если РежимОтображения <> Неопределено Тогда
		Элемент.РежимОтображения	= РежимОтображения;
	КонецЕсли; 
	Если Заголовок <> Неопределено Тогда
		Элемент.Заголовок	= Заголовок;
	КонецЕсли; 
	
КонецПроцедуры //ОтборЗначениеУстановить 

// Сбрасывает отборы
//
// Параметры:  
//
Процедура ОтборыСбросить(Отбор) Экспорт
	
	Для Каждого ОтборЭлемент Из Отбор.Элементы Цикл
		ОтборЭлемент.Использование	= Ложь;
	КонецЦикла;
	
КонецПроцедуры //ОтборыСбросить 

// Очищает коллекцию элементов отбора
//
// Параметры:  
// 	Отбор
//
Процедура ОтборОчистить(Отбор) Экспорт
	
	Пока Отбор.Элементы.Количество() Цикл
		Отбор.Элементы.Удалить(0);
	КонецЦикла;
	
КонецПроцедуры //ОтборОчистить 

