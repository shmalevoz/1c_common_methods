﻿// 
// Клиентские методы работы с метаданными
// 

// Возвращает результат выбора объекта метаданных
//
// Параметры:  
// 	Объект - объект получатель оповещения
// 	Метод - метод получаемого оповещения о выборе
// 	Адаптер - адаптер работы с метаданными
// 	КлассыСписок - список доступных для выбора классов. Имена классов по он_Типы.ТипXXXИмя
// 	ПодсистемыФильтрИспользовать - флаг использования фильтра по подсистемам
// 	ПодсистемыФильтрВидимыеТолько - флаг включения только отображаемых в командном интерфейсе подсистем
//
// Возвращаемое значение: 
// 	Структура|Неопределено
//
Функция ОбъектВыбрать(Объект
	, Метод
	, Адаптер = Неопределено
	, КлассыСписок = Неопределено
	, ПодсистемыФильтрИспользовать = Истина
	, ПодсистемыФильтрВидимыеТолько = Ложь
	, ИменаОтборПрефикс = ""
	, ИменаИсключитьПрефикс = ""
	, ОповещениеПараметры = Неопределено) Экспорт
	
	АдаптерИмя	= он_Метаданные.ОбъектВыборАдаптерИмя();
	
	Если НЕ ПустаяСтрока(АдаптерИмя) Тогда
		Параметры	= Новый Структура;
		Параметры.Вставить("МетаданныеАдаптер", Адаптер);
		Параметры.Вставить("КлассыСписок", КлассыСписок);
		Параметры.Вставить("ПодсистемыФильтрИспользовать", ПодсистемыФильтрИспользовать);
		Параметры.Вставить("ПодсистемыФильтрВидимыеТолько", ПодсистемыФильтрВидимыеТолько);
		Параметры.Вставить("ИменаОтборПрефикс", ИменаОтборПрефикс);
		Параметры.Вставить("ИменаИсключитьПрефикс", ИменаИсключитьПрефикс);
		он_ДиалогКлиент.ФормаОткрытьМодально(Объект
		, Метод
		, АдаптерИмя + ".Форма"
		, Параметры
		, 
		, 
		, ОповещениеПараметры);
	КонецЕсли;
	
КонецФункции //ОбъектВыбрать 