﻿
// Предопределенный метод
// 
&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	он_ДиалогСервер.ФормаАдминистративнаяПриСозданииНаСервере(ЭтаФорма
	, Отказ
	, СтандартнаяОбработка);
	
	Если НЕ Отказ Тогда
		ЭтаФорма.Параметры.Свойство("Адаптер", Адаптер);
		ПараметрыТаблицаЗаполнитьНаСервере();
	КонецЕсли;
	
КонецПроцедуры

// Заполнение таблица параметров адаптера
//
// Параметры:  
//
&НаСервере 
Процедура ПараметрыТаблицаЗаполнитьНаСервере()
	
	ПараметрыТаблица.Очистить();
	
	Если ЗначениеЗаполнено(Адаптер) Тогда
		Для Каждого Элемент Из он_АдаптерПараметры.Получить(Адаптер) Цикл
			ПараметрыСтрока	= ПараметрыТаблица.Добавить();
			ПараметрыСтрока.Параметр	= Элемент.Ключ;
			ПараметрыСтрока.Значение	= Элемент.Значение;
			ПараметрыСтрока.Эталон		= ПараметрыСтрока.Значение;
		КонецЦикла;
	КонецЕсли;
	
КонецПроцедуры //ПараметрыТаблицаЗаполнитьНаСервере 

// Выполняет запись параметров адаптера
//
// Параметры:  
//
&НаСервере 
Процедура ЗаписатьНаСервере()
	
	Если ЗначениеЗаполнено(Адаптер) Тогда
		АдаптерПараметры	= Новый Соответствие;
		Для Каждого ПараметрыСтрока Из ПараметрыТаблица Цикл
			АдаптерПараметры.Вставить(ПараметрыСтрока.Параметр, ПараметрыСтрока.Значение);
		КонецЦикла;
		он_АдаптерПараметры.Установить(Адаптер, АдаптерПараметры);
	КонецЕсли;
	
КонецПроцедуры //ЗаписатьНаСервере 

// Запись данных
//
// Параметры:  
//
&НаКлиенте 
Процедура ДанныеЗаписать() Экспорт
	
	ЗаписатьНаСервере();
	ЭтаФорма.Модифицированность	= Ложь;
	
КонецПроцедуры //ДанныеЗаписать 

// Предопределенный метод
// 
&НаКлиенте
Процедура ПриОткрытии(Отказ)
	
	ЭлементыПараметрыУстановить();
	
	Элементы.Адаптер.ТолькоПросмотр	= ЗначениеЗаполнено(Адаптер);
	
КонецПроцедуры

// Устанавливает параметры элементов формы
//
// Параметры:  
//
&НаКлиенте 
Процедура ЭлементыПараметрыУстановить()
	
	Элементы.ПараметрыТаблица.ТолькоПросмотр	= НЕ ЗначениеЗаполнено(Адаптер);
	
КонецПроцедуры //ЭлементыПараметрыУстановить 

// При изменении адаптера
// 
&НаКлиенте
Процедура АдаптерПриИзменении(Элемент)
	
	ЭлементыПараметрыУстановить();
	ПараметрыТаблицаЗаполнитьНаСервере();
	
КонецПроцедуры

// Предопределенный метод
// 
&НаКлиенте
Процедура ПередЗакрытием(Отказ, СтандартнаяОбработка)
	
	он_ДиалогКлиент.ФормаПередЗакрытиемМодифицированность(ЭтаФорма, "ДанныеЗаписать", Отказ);
	
КонецПроцедуры

// По команде Записать
// 
&НаКлиенте
Процедура Записать(Команда)
	
	ДанныеЗаписать();
	
КонецПроцедуры

// По команде Записать и закрыть
// 
&НаКлиенте
Процедура ЗаписатьИЗакрыть(Команда)
	
	Записать(Неопределено);
	ЭтаФорма.Закрыть();
	
КонецПроцедуры

// При активации строки таблицы параметров
// 
&НаКлиенте
Процедура ПараметрыТаблицаПриАктивизацииСтроки(Элемент)
	
	ДанныеТекущие	= Элементы.ПараметрыТаблица.ТекущиеДанные;
	
	Если ДанныеТекущие <> Неопределено Тогда
		Элементы.ПараметрыТаблицаЗначение.ОграничениеТипа	= Новый ОписаниеТипов(он_Коллекции.ЗначениеМассив(ТипЗнч(ДанныеТекущие.Эталон)));
	КонецЕсли;
	
КонецПроцедуры
